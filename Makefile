PANDA_DIR=../panda3d-1.8.1
PANDA_BUILT_DIR=../panda3d-1.8.1/built
PANDA_LIBS=-lp3framework -lpanda -lpandaexpress -lp3dtoolconfig -lp3dtool

run-planets: all
	./launch.sh

all: planets launch.sh

planets: planets.o
	g++ -o planets planets.o --std=c++11 \
		-L ${PANDA_BUILT_DIR}/lib/ ${PANDA_LIBS} -pthread

planets.o: planets.cxx Makefile
	g++ -c -o planets.o planets.cxx --std=c++11 \
		-fPIC -I ${PANDA_BUILT_DIR}/include/

launch.sh: Makefile
	echo '#!/bin/bash' > launch.sh
	echo 'set -e' >> launch.sh
	echo 'set -x' >> launch.sh
	echo 'cd $$(dirname $$0)' >> launch.sh
	echo "LD_LIBRARY_PATH=${PANDA_BUILT_DIR}/lib/" >> launch.sh
	echo "./planets" >> launch.sh
	chmod a+x launch.sh

build-panda3d:
	cd ${PANDA_DIR} && python makepanda/makepanda.py --threads 2 --nothing \
		--use-gl \
		--use-x11 \
		--use-png \
		--use-jpeg
		#--use-openal \
		#--use-pandafx \
		#--use-pandaparticlesystem \
		#--use-skel \
		#--use-direct \
