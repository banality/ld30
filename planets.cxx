#include "pandaFramework.h"
#include "pandaSystem.h"
#include "geomTriangles.h"
#include "ambientLight.h"
#include "directionalLight.h"
#include "randomizer.h"

#include <cmath>
#include <iostream>
#include <initializer_list>

char const* title = "Connected Planets";
const int disc_size = 100;
NodePath camera;

void setup_lighting(NodePath np) {
    PT(DirectionalLight) sun = new DirectionalLight("sun");
    sun->set_color(LVecBase4f(0.8, 0.8, 0.8, 1));
    NodePath sun_node = np.attach_new_node(sun);
    sun_node.set_hpr(-30, -60, 0);
    np.set_light(sun_node);

    PT(AmbientLight) ambient = new AmbientLight("ambient");
    ambient->set_color(LVecBase4f(0.5, 0.5, 0.5, 1));
    NodePath ambient_node = np.attach_new_node(ambient);
    np.set_light(ambient_node);
}

void add_triangle(Geom* geom, int p1, int p2, int p3) {
    PT(GeomTriangles) prim = new GeomTriangles(Geom::UH_static);
    prim->add_vertices(p1, p2, p3);
    geom->add_primitive(prim);
}

void add_rectangle(Geom* geom, int p1, int p2, int p3, int p4) {
    PT(GeomTriangles) prim = new GeomTriangles(Geom::UH_static);
    prim->add_vertices(p1, p2, p4);
    geom->add_primitive(prim);

    prim = new GeomTriangles(Geom::UH_static);
    prim->add_vertices(p4, p2, p3);
    geom->add_primitive(prim);
}

PT(GeomNode) generate_disc(int points, float radius, float height) {
    // https://www.panda3d.org/manual/index.php/Pre-defined_vertex_formats
    CPT(GeomVertexFormat) format;
    format = GeomVertexFormat::get_v3n3c4();

    //
    // Create the vertices
    //

    PT(GeomVertexData) vdata;
    vdata = new GeomVertexData("planet", format, Geom::UH_static);

    // 2 faces, points around the parameter + point at centre.
    // (don't actually use the bottom centre point).
    vdata->set_num_rows(2*(points + 1));

    GeomVertexWriter vertex(vdata, "vertex");
    GeomVertexWriter normal(vdata, "normal");
    GeomVertexWriter color (vdata, "color");

    int vertex_count = 0;

    vertex.add_data3f(0, 0, 0);
    normal.add_data3f(0, 0, 1);
    color.add_data4f(0, 0.5, 0, 1);

    vertex.add_data3f(0, 0, -height);
    normal.add_data3f(0, 0, -1);
    color.add_data4f(0, 1, 0, 1);

    for (int i = 0; i < points; ++i) {
        const double angle = M_PI * (2 * i) / points;
        const double angle2 = M_PI * (2 * i + 1) / points;

        vertex.add_data3f(radius * std::sin(angle), radius * std::cos(angle), 0);
        normal.add_data3f(std::sin(angle), std::cos(angle), 1);
        color.add_data4f(0, 0.5 + std::sin(angle*13) / 4, 0, 1);

        vertex.add_data3f(radius * std::sin(angle2), radius * std::cos(angle2), -height);
        normal.add_data3f(std::sin(angle2), std::cos(angle2), -1);
        color.add_data4f(0, 0.25, 0, 0);
    }

    //
    // Create geometry and fill with triangles
    // Only doing top and sides of disc.
    //

    PT(Geom) geom = new Geom(vdata);

    for (int i = 0; i < points; ++i) {
        int j = (i + 1) % points;
        add_triangle(geom, 0, 2+j*2, 2+i*2);
    }

    for (int i = 0; i < points; ++i) {
        int j = (i + 1) % points;
        add_triangle(geom, 2+i*2, 2+j*2, 3+i*2);
        add_triangle(geom, 3+j*2, 3+i*2, 2+j*2);
    }

    //
    // Add geometry to GeomNode.
    //

    PT(GeomNode) node = new GeomNode("gnode");
    node->add_geom(geom);
    return node;
}

struct ThingTriangle {
    struct Points {
        int p1, p2, p3;
    };

    LVecBase3f normal;
    LVecBase4f colour;
    Points points;
};

PT(GeomNode) generate_thing(
        char const* name,
        std::initializer_list<LVecBase3f> vertices,
        std::initializer_list<ThingTriangle> triangles) {

    CPT(GeomVertexFormat) format;
    format = GeomVertexFormat::get_v3n3c4();

    PT(GeomVertexData) vdata;
    vdata = new GeomVertexData(name, GeomVertexFormat::get_v3n3c4(), Geom::UH_static);

    GeomVertexWriter vertex(vdata, "vertex");
    GeomVertexWriter normal(vdata, "normal");
    GeomVertexWriter color (vdata, "color");

    std::vector<LVecBase3f> v(vertices);
    int index = 0;
    for (auto const& t : triangles) {
        int i = index;
        index += 3;

        vertex.add_data3f(v[t.points.p1]);
        vertex.add_data3f(v[t.points.p2]);
        vertex.add_data3f(v[t.points.p3]);
        normal.add_data3f(t.normal);
        normal.add_data3f(t.normal);
        normal.add_data3f(t.normal);
        color.add_data4f(t.colour);
        color.add_data4f(t.colour);
        color.add_data4f(t.colour);
    }

    PT(Geom) geom = new Geom(vdata);
    for (int i = 0; i < index; i += 3) {
        add_triangle(geom, i, i + 1, i + 2);
    }

    PT(GeomNode) node = new GeomNode("gnode");
    node->add_geom(geom);
    return node;
}

PT(GeomNode) generate_robot() {
    float leg_gap = 0.1;
    float arm_gap = 0.1;
    float leg_width = 1;
    float arm_width = 0.8;
    float head_width = 1;
    float head_height = 1.2;

    float head_left = -head_width / 2, head_right = head_width / 2;
    float head_bottom = 5, head_top = head_bottom + head_height;
    float front_y = 0.5, back_y = -0.5;
    float leg_left2 = -leg_gap / 2, leg_left1 = leg_left2 - leg_width;
    float leg_right1 = leg_gap / 2, leg_right2 = leg_right1 + leg_width;
    float arm_left2 = leg_left1 - arm_gap, arm_left1 = arm_left2 - arm_width;
    float arm_right1 = leg_right2 + arm_gap, arm_right2 = arm_right1 + arm_width;
    LVecBase3f front = {0, 1, 0};
    LVecBase3f back = {0, -1, 0};
    LVecBase3f top = {0, 0, 1};
    LVecBase3f bottom = {0, 0, -1};
    LVecBase3f left = {-1, 0, 0};
    LVecBase3f right = {1, 0, 0};

    LVecBase4f colour = {0.5, 0.5, 0.5, 1};
    LVecBase4f colour2 = {0.75, 0, 0, 1};

    return generate_thing("robot", {
        {head_left, back_y, head_top}, {head_right, back_y, head_top}, // 0
        {arm_left1, back_y, head_bottom}, {head_left, back_y, head_bottom}, {head_right, back_y, head_bottom}, {arm_right2, back_y, head_bottom}, // 2
        {arm_left2, back_y, 4}, {leg_left1, back_y, 4}, {leg_right2, back_y, 4}, {arm_right1, back_y, 4}, // 6
        {arm_left1, back_y, 2}, {arm_left2, back_y, 2}, {arm_right1, back_y, 2}, {arm_right2, back_y, 2}, // 10
        {leg_left2, back_y, 2.5}, {leg_right1, back_y, 2.5}, // 14
        {leg_left1, back_y, 0}, {leg_left2, back_y, 0}, {leg_right1, back_y, 0}, {leg_right2, back_y, 0}, // 16

        {head_left, front_y, head_top}, {head_right, front_y, head_top}, // 20
        {arm_left1, front_y, head_bottom}, {head_left, front_y, head_bottom}, {head_right, front_y, head_bottom}, {arm_right2, front_y, head_bottom}, // 22
        {arm_left2, front_y, 4}, {leg_left1, front_y, 4}, {leg_right2, front_y, 4}, {arm_right1, front_y, 4}, // 26
        {arm_left1, front_y, 2}, {arm_left2, front_y, 2}, {arm_right1, front_y, 2}, {arm_right2, front_y, 2}, // 30
        {leg_left2, front_y, 2.5}, {leg_right1, front_y, 2.5}, // 34
        {leg_left1, front_y, 0}, {leg_left2, front_y, 0}, {leg_right1, front_y, 0}, {leg_right2, front_y, 0}, // 36

        {0,0,0}
    }, {
        {back, colour, {3, 1, 0}}, {back, colour, {4, 1, 3}},
        {back, colour, {3, 2, 7}}, {back, colour, {5, 4, 8}},
        {back, colour, {6, 7, 2}}, {back, colour, {8, 9, 5}},
        {back, colour, {10, 6, 2}}, {back, colour, {9, 13, 5}},
        {back, colour, {10, 11, 6}}, {back, colour, {12, 13, 9}},
        {back, colour, {7, 14, 3}}, {back, colour, {15, 8, 4}},
        {back, colour, {3, 14, 4}}, {back, colour, {4, 14, 15}},
        {back, colour, {16, 14, 7}}, {back, colour, {15, 19, 8}},
        {back, colour, {16, 17, 14}}, {back, colour, {18, 19, 15}},

        {front, colour2, {21, 23, 20}}, {front, colour2, {21, 24, 23}},
        {front, colour2, {22, 23, 27}}, {front, colour2, {24, 25, 28}},
        {front, colour2, {27, 26, 22}}, {front, colour2, {29, 28, 25}},
        {front, colour2, {26, 30, 22}}, {front, colour2, {33, 29, 25}},
        {front, colour2, {31, 30, 26}}, {front, colour2, {33, 32, 29}},
        {front, colour2, {34, 27, 23}}, {front, colour2, {28, 35, 24}},
        {front, colour2, {34, 23, 24}}, {front, colour2, {34, 24, 35}},
        {front, colour2, {34, 36, 27}}, {front, colour2, {39, 35, 28}},
        {front, colour2, {37, 36, 34}}, {front, colour2, {39, 38, 35}},

        {top, colour, {0, 1, 20}}, {top, colour, {21, 20, 1}},
        {top, colour, {2, 3, 22}}, {top, colour, {23, 22, 3}},
        {top, colour, {4, 5, 24}}, {top, colour, {25, 24, 5}},

        {left, colour, {3, 0, 20}}, {left, colour, {20, 23, 3}},
        {right, colour, {1, 4, 21}}, {right, colour, {24, 21, 4}},

        {left, colour, {10, 2, 30}}, {left, colour, {22, 30, 2}},
        {right, colour, {5, 13, 25}}, {right, colour, {33, 25, 13}},

        {left, colour, {16, 7, 36}}, {left, colour, {27, 36, 7}},
        {right, colour, {8, 19, 28}}, {right, colour, {39, 28, 19}},

        {left, colour, {18, 15, 38}}, {left, colour, {35, 38, 15}},
        {right, colour, {14, 17, 34}}, {right, colour, {37, 34, 17}},

        {left, colour, {12, 9, 32}}, {left, colour, {29, 32, 9}},
        {right, colour, {6, 11, 26}}, {right, colour, {31, 26, 11}}
    });
}

// Keyboard controls

bool left_pressed = false;
bool right_pressed = false;
bool up_pressed = false;
bool down_pressed = false;
bool fire_pressed = false;

void event_move_key(const Event* event, void* data) {
    *static_cast<bool*>(data) = true;
}

void event_move_key_up(const Event* event, void* data) {
    *static_cast<bool*>(data) = false;
}

// Task Manager

PT(AsyncTaskManager) taskMgr = AsyncTaskManager::get_global_ptr();
PT(ClockObject) globalClock = ClockObject::get_global_clock();

struct Movement {
    float x;
    float y;
    int dir;
};

const float speed45 = 1 / std::sqrt(2);

// 812
// 703
// 654
Movement moves[] = {
    {0, 0, 0},
    {0, 1, 0},
    {speed45, speed45, 315},
    {1, 0, 270},
    {speed45, -speed45, 225},
    {0, -1, 180},
    {-speed45, -speed45, 135},
    {-1, 0, 90},
    {-speed45, speed45, 45}
};

// 1 = left
// 2 = right
// 4 = down
// 8 = up
int move_index[16] = {
    0, 7, 3, 0,
    5, 6, 4, 0,
    1, 8, 2, 0,
    0, 0, 0, 0
};

Movement const& get_direction(LVecBase3 const& from, LVecBase3 const& to) {
    const float epsilon = 0.1;
    float x = to[0] - from[0];
    float y = to[1] - from[1];
    int right = x > epsilon ? 2 : x < -epsilon ? 1 : 0;
    int up = y > epsilon ? 8 : y < -epsilon ? 4 : 0;
    x = std::abs(x);
    y = std::abs(y);
    if (x * 8 < y) { right = 0; }
    if (y * 8 < x) { up = 0; }
    return moves[move_index[right + up]];
}

struct GameState {
    static const int create_robot_ticks = 5;
    static const int create_fire_ticks = 5;

    PandaFramework framework;
    WindowFramework* window;
    NodePath render;
    Randomizer randomizer;
    NodePath player_robot;
    std::vector<NodePath> other_robots;
    int robot_ticks = 0;
    int fire_ticks = 0;
    bool dead = false;

    AsyncTask::DoneStatus task(GenericAsyncTask*);
    void movePlayer();
    void moveRobots();
    void createRobots();

    static AsyncTask::DoneStatus taskTrampoline(GenericAsyncTask* task, void* data) {
        return static_cast<GameState*>(data)->task(task);
    }
};

AsyncTask::DoneStatus GameState::task(GenericAsyncTask*) {
    double time = globalClock->get_real_time();
    movePlayer();
    moveRobots();
    createRobots();
    if (dead) { framework.close_window(window); exit(0); }
    return dead ? AsyncTask::DS_exit : AsyncTask::DS_cont;
}

void GameState::movePlayer() {
    float speed = 0.4;

    int index = move_index[1 * left_pressed + 2 * right_pressed + 4 * down_pressed + 8 * up_pressed];
    if (index) {
        Movement m = moves[index];
        auto x = player_robot.get_x() + m.x * speed;
        auto y = player_robot.get_y() + m.y * speed;
        auto distance2 = x*x + y*y;
        if (distance2 > (disc_size-1)*(disc_size-1)) {
            auto m = (disc_size-1) / std::sqrt(distance2);
            x *= m;
            y *= m;
        }
        player_robot.set_x(x);
        player_robot.set_y(y);
        player_robot.set_h(m.dir);
    }

    if (fire_pressed) {
        if (fire_ticks == 0) {
            fire_ticks = create_fire_ticks;


        }
    }
    if (fire_ticks) { --fire_ticks; }
}

void GameState::moveRobots() {
    auto player_pos = player_robot.get_pos();
    for (auto& r: other_robots) {
        auto&& d = get_direction(r.get_pos(), player_pos);
        r.set_h(d.dir);
        float speed = 0.4;
        r.set_x(r.get_x() + d.x * speed);
        r.set_y(r.get_y() + d.y * speed);

        auto dx = r.get_x() - player_pos.get_x();
        auto dy = r.get_y() - player_pos.get_y();
        if (dx * dx + dy * dy < 4) {
            dead = true;
        }
    }
}

void GameState::createRobots() {
    if (++robot_ticks == create_robot_ticks && other_robots.size() < 50) {
        robot_ticks = 0;
        int angle = randomizer.random_int(360);
        double radians = M_PI * angle / 180;
        int x = -(disc_size-1) * std::sin(radians);
        int y = (disc_size-1) * std::cos(radians);
        NodePath r = render.attach_new_node(generate_robot());
        r.set_pos(x,y,0);
        auto direction = get_direction(r.get_pos(), player_robot.get_pos());
        r.set_h(direction.dir);
        other_robots.push_back(r);
    }
}

// Main

int main(int argc, char *argv[]) {
    GameState g;

    g.framework;
    g.framework.open_framework(argc, argv);
    g.framework.set_window_title(title);
    g.framework.set_background_type(WindowFramework::BT_black);
    g.window = g.framework.open_window();
    g.render = g.window->get_render();

    // Keyboard
    g.window->enable_keyboard();
    g.framework.define_key("w", "move up", &event_move_key, &up_pressed);
    g.framework.define_key("w-up", "move up", &event_move_key_up, &up_pressed);
    g.framework.define_key("s", "move down", &event_move_key, &down_pressed);
    g.framework.define_key("s-up", "move down", &event_move_key_up, &down_pressed);
    g.framework.define_key("a", "move left", &event_move_key, &left_pressed);
    g.framework.define_key("a-up", "move left", &event_move_key_up, &left_pressed);
    g.framework.define_key("d", "move right", &event_move_key, &right_pressed);
    g.framework.define_key("d-up", "move right", &event_move_key_up, &right_pressed);
    g.framework.define_key("space", "fire", &event_move_key, &fire_pressed);
    g.framework.define_key("space-up", "fire", &event_move_key_up, &fire_pressed);

    // Lighting
    setup_lighting(g.render);

    // Create a objects.
    NodePath disc = g.render.attach_new_node(generate_disc(100, disc_size, 1));
    disc.set_pos(0, 0, 0);

    g.player_robot = g.render.attach_new_node(generate_robot());
    g.player_robot.set_pos(0, 0, 0);

    // Camera
    camera = g.window->get_camera_group();
    camera.set_pos(0, -100, 50);
    camera.set_hpr(0, -20, 0);

    taskMgr->add(new GenericAsyncTask("Game task", &GameState::taskTrampoline, (void*) &g));

    // Loop
    g.framework.main_loop();
    g.framework.close_framework();
    return (0);
}
